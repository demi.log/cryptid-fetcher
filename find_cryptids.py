"""Fetch cryptids from author's artstation profile page.
Use Selenium for loading paginated images.
Save only links and descriptions, not images themselves.
"""
from json.decoder import JSONDecodeError
import time
import json

from tqdm import tqdm
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup

# Try to fetch less images from other source and display their names
DEBUG = False

# Artstation Porfolio with endless scroll
URL = "https://www.artstation.com/piperthibodeau"
PATH = "cryptids.json"
CRYPTIDS = {}

# URL for testing purposes, with only a couple of pages
if DEBUG:
    URL = "https://www.artstation.com/kangm17"


def get_driver():
    """Configure Chrome diver (check the PATH if it does not work)"""
    driver = webdriver.Chrome(ChromeDriverManager().install())
    options = webdriver.ChromeOptions()
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--incognito")
    options.add_argument("--headless")

    return webdriver.Chrome(options=options)


def scroll(driver, timeout):
    """Scroll down the page, then wait for content to load"""
    total_pages = 1
    scroll_pause_time = timeout
    tic = time.perf_counter()

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    print("Parsing pages: ")

    while True:
        print(total_pages, sep=" ", end=" ", flush=True)

        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(scroll_pause_time)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            # If heights are the same it will exit the function
            break

        last_height = new_height
        total_pages += 1

    toc = time.perf_counter()
    print(f"\nParsing done in {toc - tic:0.4f} seconds")


def save_cryptid(image):
    """Save link to image and its name if it's not already in the DB"""
    src = image.get("src")
    name = image.get("alt")
    if name not in CRYPTIDS:
        # Get link to a larger image and save to JSON dict
        CRYPTIDS[name] = src.replace("smaller_square", "large")
        if DEBUG:
            print(f"Saved {name}")


def get_cryptids(driver):
    """Try to load all the cryptids from dynamically updated page"""
    # Wait before throwing an exception
    driver.implicitly_wait(10)
    # Open page for inital loading
    driver.get(URL)
    # Start scrolling
    scroll(driver, 5)
    # Parse HTML
    soup = BeautifulSoup(driver.page_source, "html.parser")
    # We no longer need the driver
    driver.close()

    # Add each NEW image to dictionary, saving its alt and url
    for image in tqdm(soup.find_all("img", class_="image")):
        save_cryptid(image)


def load_cryptids():
    """Load already saved images from JSON"""
    try:
        with open(PATH, "r", encoding="utf-8") as f:
            CRYPTIDS = json.load(f)
    except (JSONDecodeError, IOError):
        CRYPTIDS = {}

    print(f"Total cryptids loaded: {len(CRYPTIDS)}")


def update_cryptids():
    """Update JSON with new images"""
    with open(PATH, "w", encoding="utf-8") as f:
        json.dump(CRYPTIDS, f)

    print(f"Total cryptids saved: {len(CRYPTIDS)}")


if __name__ == "__main__":
    load_cryptids()
    driver = get_driver()
    get_cryptids(driver)
    update_cryptids()
