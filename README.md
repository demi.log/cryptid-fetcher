# Cryptid Fetcher

Fetches all the links to images from https://www.artstation.com/piperthibodeau and saves alts:links as JSON dictionary.

Should be used in conjunction with some bot/web page that will randomly generate new url to cryptid on deman/refresh.